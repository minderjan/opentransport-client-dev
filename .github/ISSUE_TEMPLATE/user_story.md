---
name: User Story
about: Create a user story with definition of done and acceptance criteria.
title: "[]"
labels: story
assignees: minderjan

---

{summary}

__As a__ {acteur},
__I want to__ {functionality},
__so that__ {benefit}.

**Acceptance Criterias**
- [ ] ...

**Definition of Done**
- [ ] Acceptance criteria fulfilled
- [ ] All potential errors are handled
- [ ] Debug messages are implemented
- [ ] Functionality will be tested automatically
- [ ] Methods are documented
- [ ] The function can be run in parallel 