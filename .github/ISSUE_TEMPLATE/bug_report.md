---
name: Bug report
about: Create a report to help us improve
title: "[BUG]"
labels: bug
assignees: minderjan

---

**Describe the bug**
A clear and concise description of what the bug is.

**To Reproduce**
Steps to reproduce the behavior:
1. Prepared Request '...'
2. Call Library Method '....'
3. See error

**Expected behavior**
A clear and concise description of what you expected to happen.

**Implementation**
The source code of your implementation.

**Logs**
Add logs from your logger or output. Enable the Debug Logger to view more detailed messages.

**Desktop (please complete the following information):**
 - OS: [e.g. Ubuntu 18.04.4 LTS]
 - Go Version: [e.g. 1.13]
 - Module Version: [e.g. 1.0.2]

**Additional context**
Add any other context about the problem here.
