package main

import (
	"context"
	"fmt"
	"gitlab.com/minderjan/opentransport-client-dev/opentransport"
	"os"
	"time"
)

func main() {

	station := "Zürich, Schmiede Wiedikon"
	when := time.Now()

	// Advanced filter options
	stbOpts := opentransport.StbOpts{
		Transportations: []opentransport.Transportation{opentransport.Bus},
		DateTime:        when,
		Arrival:         true,
		Limit:           2,
	}

	client := opentransport.NewClient()
	client.EnableLogs(nil)
	result, err := client.Stationboard.SearchWithOpts(context.Background(), station, stbOpts)
	if err != nil {
		fmt.Printf("Could not get Stationboard for %s: %s", station, err)
		os.Exit(1)
	}

	for _, c := range result.Connections {
		fmt.Printf("Arrival at %s (%s)%s to %s\n",
			c.Stop.Departure.Time.Format("15:04"),
			c.Category,
			c.Number,
			c.To)
	}

}
