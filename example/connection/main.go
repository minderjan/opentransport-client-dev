package main

import (
	"context"
	"fmt"
	"gitlab.com/minderjan/opentransport-client-dev/opentransport"
	"os"
	"time"
)

func main() {

	from := "Zürich HB"
	to := "Bern"
	when := time.Now()

	client := opentransport.NewClient()
	result, err := client.Connection.Search(context.Background(), from, to, when)

	if err != nil {
		fmt.Printf("Failed to search connection from %s to %s at %s: %s",  from, to, when.Format("2006-01-02 15:04"), err)
		os.Exit(1)
	} else {
		for _, c := range result.Connections {

			// get first part of the connection
			s := c.Sections[0]

			fmt.Printf("%s %s at %s on platform %s \n",
				s.Journey.Category,
				s.Journey.Number,
				s.Departure.Departure.Time.Format("15:04"),
				s.Departure.Platform)
		}
	}

}