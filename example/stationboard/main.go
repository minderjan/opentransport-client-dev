package main

import (
	"context"
	"fmt"
	"gitlab.com/minderjan/opentransport-client-dev/opentransport"
	"os"
)

func main() {

	station := "Zürich HB"

	client := opentransport.NewClient()
	result, err := client.Stationboard.Search(context.Background(), station)
	if err != nil {
		fmt.Printf("Could not get Stationboard for %s: %s", station, err)
		os.Exit(1)
	}

	for _, c := range result.Connections {
		fmt.Printf("Departure at %s (%s)%s to %s\n",
			c.Stop.Departure.Time.Format("15:04"),
			c.Category,
			c.Number,
			c.To)
	}

}
