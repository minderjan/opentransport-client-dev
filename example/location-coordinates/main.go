package main

import (
	"context"
	"fmt"
	"gitlab.com/minderjan/opentransport-client-dev/opentransport"
	"os"
)

func main() {

	loc := "Zürich Bürkliplatz"

	client := opentransport.NewClient()
	locations, err := client.Location.Search(context.Background(), loc)
	if err != nil {
		fmt.Printf("Could not search location for %s: %s", loc, err)
		os.Exit(1)
	}

	for _, s := range locations {
		if len(s.Id) > 0 {
			fmt.Printf("Station: %s\n", s.Name)
		} else {
			fmt.Printf("Address: %s\n", s.Name)
		}
	}

}