# OpenTransport Client

[![CircleCI](https://circleci.com/gh/minderjan/opentransport-client-dev/tree/master.svg?style=svg&circle-token=9a801ad8c7b932e9b21dfc63a1b3de30ebe7632c)](https://circleci.com/gh/minderjan/opentransport-client-dev/tree/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=minderjan_opentransport-client-dev&metric=alert_status&token=fdc7f793caa9f7296aa03ee7fc0b8f3a6f4851e5)](https://sonarcloud.io/dashboard?id=minderjan_opentransport-client-dev)

The OpenTransport Client offers a simplified integration of the data from [transport.opendata.ch](https://transport.opendata.ch).

The docs of the implemented API can be found here: https://transport.opendata.ch/docs.html


* [Usage](#usage)
* [Examples](#examples)
* [Development](#development)
* [Contribution](#contribution)
* [Changelog](#changelog)
* [License](#license)
* [Author](#author)

## Install
You need a working golang 1.14+ environment on your machine. If this is not the case, use this guide to [getting started](https://golang.org/doc/install).
```bash
go get -v gitlab.com/minderjan/opentransport-client-dev/opentransport
```

## Usage

The library is separated into different services. These are accessible via an instance of the OpenTransport Client.

![overview](.github/services.png)


### Client
```go
package main

import "gitlab.com/minderjan/opentransport-client-dev/opentransport"

func main() {
    client := opentransport.NewClient()
}
```

### Location
```go
locations, _ := client.Location.Search(context.Background(), "Zürich Bürkliplatz")
for _, s := range locations {
    fmt.Printf("%s\n", s.Name)
}
```

### Connection

```go
result, _ := client.Connection.Search(context.Background(), "Zürich HB", "Bern", time.Now())

for _, c := range result.Connections {
    s := c.Sections[0] // first part of the connection
    fmt.Printf("%s %s at %s on platform %s\n", s.Journey.Category, s.Journey.Number, s.Departure.Departure.Time.Format("15:04"), s.Departure.Platform)
}
```

### Stationboard

```go
result, _ := client.Stationboard.Search(context.Background(), "Zürich HB")
for _, c := range result.Connections {
    fmt.Printf("Departure at %s (%s)%s to %s\n", c.Stop.Departure.Time.Format("15:04"), c.Category, c.Number, c.To)
}
```

## Development

1. Setup a local [Golang Environment](https://golang.org/doc/install). Use Go 1.14+
2. Clone the repository somewhere on your machine or put it in your [GOPATH](https://golang.org/doc/gopath_code.html)

### Development methods
There are multiple examples available in the `examples/` directory
```
go run dev/main.go
```

### Run Tests
Tests can be executed within the root directory of this repository.
```
go test -v -covermode=count -coverprofile=bin/coverage.out ./opentransport
```
Visualize the Code Coverage as Website
```
go tool cover -html=bin/coverage.out
```

### Logging
The library does not produce log messages by default. However, this can be adjusted. You can either enable logging with an empty io.Writer to write to os.stdout.You can also define your own output, for example to write the logs to a file. 

```go
// Write to stdout and stderr
client.EnableLogs(nil)

// Create a logfile
f, _ := os.OpenFile("opentransport.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
// Create new multi writer to write the logs to the file and to stdout in parallel
multi := io.MultiWriter(f, os.Stdout)
client.EnableLogs(multi)
```

## Contribution

## Changelog

* _v0.1.0_ Initial Version

## License
This software is licensed under a MIT license

## Author
Created by [Jan Minder](https://github.com/minderjan)